<footer class="footer">
	<div class="contain">
		<?php
			wp_nav_menu( array(
				'theme_location' => 'footer',
				'menu_class' => 'nav footer-nav',
				"title_li" => false,
				"container" => false
			) );
		?>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
