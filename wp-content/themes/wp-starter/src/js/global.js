jQuery(function($) {

// -----------------------------------------------------------------------------
//! Toggle nav
// -----------------------------------------------------------------------------

	$( ".toggle-nav" ).on( "click", function() {
		$( "body" ).toggleClass( "show-nav" );
	});

	$( ".nav-backdrop, .close-nav" ).on( "click", function() {
		$( "body" ).removeClass( "show-nav" );
	});

});
