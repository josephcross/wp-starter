<?php get_header();
	while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'inc/modules/hero' ); ?>

	<section class="post-content">
		<div class="contain">
			<main>
				<?php the_content(); ?>
			</main>
			<?php get_sidebar(); ?>
		</div>
	</section>

	<?php endwhile; ?>
<?php get_footer(); ?>
