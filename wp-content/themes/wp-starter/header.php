<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8">
	<meta name="X-UA-Compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/dist/img/icons/favicon.ico" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="masthead">
	<div class="contain">

		<a href="<?php bloginfo( 'url' ); ?>" class="logo">
			<!-- <img src="<?php bloginfo('template_url'); ?>/dist/img/logos/logo-" alt=""> -->
			<img src="http://placehold.it/140x60" alt="">
		</a>

		<a class="toggle-nav" href="#">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</a>

		<?php
			wp_nav_menu( array(
				'theme_location' => 'header',
				'menu_class' => 'nav header-nav',
				"title_li" => false,
				"container" => false
			) );
		?>

	</div>
</header>
<div class="nav-backdrop"></div>
