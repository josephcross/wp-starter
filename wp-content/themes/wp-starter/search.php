<?php get_header(); ?>

	<section class="hero" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)">
		<div class="contain">
			<h1><?php the_title(); ?></h1>
		</div>
	</section>

	<section class="search-results">
		<div class="contain">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<h2><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></h2>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php else : ?>
				<p>There are no posts to display</p>
			<?php endif; ?>
		</div>
	</section>

<?php get_footer(); ?>
