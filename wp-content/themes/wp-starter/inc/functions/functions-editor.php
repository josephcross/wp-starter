<?php

// -----------------------------------------------------------------------------
//! Wrap iframe embeds
// -----------------------------------------------------------------------------

function THEMENAME_wrap_iframe( $html, $url, $args ) {
	return '<div class="oembed-container">' . $html . '</div>';
}
add_filter( 'embed_oembed_html', 'THEMENAME_wrap_iframe', 10, 3 );
add_filter( 'oembed_result', 'THEMENAME_wrap_iframe', 10, 3 );

?>
