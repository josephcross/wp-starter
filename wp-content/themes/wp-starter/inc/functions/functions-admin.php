<?php

// -----------------------------------------------------------------------------
//! Setup
// -----------------------------------------------------------------------------

function THEMEPREFIX_setup() {

	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );

	// Custom image sizes
	// add_image_size( 'image_size_name', 100, 100, true );

	// Register Menus
	register_nav_menus( array(
		'header' => __( 'Header', 'Header' ),
		'footer' => __( 'Footer', 'Footer' )
	) );
}
add_action( 'after_setup_theme', 'THEMEPREFIX_setup' );


// -----------------------------------------------------------------------------
//! Remove unused admin items
// -----------------------------------------------------------------------------

function THEMEPREFIX_remove_menu_items() {
    remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'THEMEPREFIX_remove_menu_items' );


// -----------------------------------------------------------------------------
//! Customize login page
// -----------------------------------------------------------------------------

function THEMEPREFIX_login_logo() { ?>
    <style type="text/css">
        #login h1 a,
		.login h1 a {
            background-image: url(<?php echo get_template_directory_uri(); ?>/dist/img/logos/TODO.svg);
			height: 44px;
			width: 284px;
			background-size: 100%;
			background-repeat: no-repeat;
	        padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'THEMEPREFIX_login_logo' );


// -----------------------------------------------------------------------------
//! Add favicon to admin pages
// -----------------------------------------------------------------------------

function THEMEPREFIX_admin_favicon() {
  	$favicon_url = get_stylesheet_directory_uri() . '/favicon.png';
	echo '<link rel="shortcut icon" href="' . $favicon_url . '">';
}
add_action( 'login_head', 'THEMEPREFIX_admin_favicon' );
add_action( 'admin_head', 'THEMEPREFIX_admin_favicon' );


// -----------------------------------------------------------------------------
//! Allow svg uploads
// -----------------------------------------------------------------------------

function THEMEPREFIX_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';
  	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


?>
