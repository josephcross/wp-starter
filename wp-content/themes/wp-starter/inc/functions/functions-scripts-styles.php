<?php

 // -----------------------------------------------------------------------------
 //! Scripts
 // -----------------------------------------------------------------------------

function THEMEPREFIX_load_scripts() {
	wp_register_script( 'THEMEPREFIX_scripts', get_template_directory_uri() . '/dist/js/theme.min.js', array('jquery'), wp_get_theme()->get('Version'), true );
	wp_enqueue_script( 'THEMEPREFIX_scripts' );
}
add_action( 'wp_enqueue_scripts', 'THEMEPREFIX_load_scripts' );


// -----------------------------------------------------------------------------
//! Styles
// -----------------------------------------------------------------------------

function THEMEPREFIX_load_styles() {
	wp_register_style( 'THEMEPREFIX_styles', get_template_directory_uri() . '/dist/css/theme.min.css', false, wp_get_theme()->get('Version') );
	wp_enqueue_style( 'THEMEPREFIX_styles' );
}
add_action( 'wp_enqueue_scripts', 'THEMEPREFIX_load_styles' );

?>
