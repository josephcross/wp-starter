<?php

function THEMEPREFIX_register_post_types() {
 	register_post_type( 'services',
     array(
       'labels' => array(
         'name' => __( 'Services' ),
         'singular_name' => __( 'Service' ),
 		'menu_name' => __( 'Services' )
       ),
       'public' => true,
       'has_archive' => true,
 	  'menu_position' => 5,
 	  'menu_icon'=> 'dashicons-clipboard',
 	  'supports' => array( 'title', 'editor' )
     )
   );
 }
add_action( 'init', 'THEMEPREFIX_register_post_types' );

?>
