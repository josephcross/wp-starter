<section class="hero" style="background-image:url(<?php echo get_the_post_thumbnail_url(); ?>)">
	<div class="contain">
		<h1><?php the_title(); ?></h1>
	</div>
</section>
